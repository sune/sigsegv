/*
 * Copyright (c) 2013 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "mainwindow.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include "testcase.h"
#include "dereferencenullpointer.h"
#include "dereferenceuninitializedpointer.h"
#include "dereferencedeletedpointer.h"
#include "deletepointer.h"
#include "assertfalse.h"
#include "brokenlist.h"

#include <QDebug>

MainWindow::MainWindow ( QWidget* parent, Qt::WindowFlags f ) : QWidget(parent,f), m_layout(new QVBoxLayout()) {
    m_layout->addWidget(new QLabel("Fancy graphics"));
    setLayout(m_layout);
}

void MainWindow::addAllTestcases() {
    QList<TestCase*> testcases;
    testcases << new DereferenceNullPointer(this) << new DereferenceUninitializedPointer(this) << new DereferenceDeletedPointer(this)
              << new DeletePointer(this) << new AssertFalse(this) << new BrokenList(this);
    Q_FOREACH(TestCase* testcase, testcases) {
        QPushButton* button = new QPushButton(testcase->name(),this);

        bool ok = connect(button, &QPushButton::clicked, testcase, &TestCase::execute);
        Q_UNUSED(ok);
        m_layout->addWidget(button);
    }

}

