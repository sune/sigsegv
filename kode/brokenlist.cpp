/*
 * Copyright (c) 2013 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "brokenlist.h"
#include <QDebug>

struct MyList {
    MyList() : next(reinterpret_cast<MyList*>(0xfeeefeee)) { }
    MyList* next;
    QString name;
};

MyList* generateList(int count) {
    if(count==0) {
        MyList* list = new MyList();
        list->name = QString::number(0);
        return list;
    }
    MyList* list = new MyList();
    list->name = QString::number(count);
    list->next = generateList(count-1);
    return list;
}


BrokenList::BrokenList ( QObject* parent ) : TestCase ( parent ) {
    setName("Broken list");
}

void BrokenList::execute() {
    MyList* first = generateList(10);
    MyList* current = first;
    while(current->next) {
        current = current->next;
    }
}



#include "brokenlist.moc"
